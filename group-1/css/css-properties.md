---
description: List of available CSS properties and examples.
---

# CSS Properties

### border-style

{% code title="border-style.css" overflow="wrap" lineNumbers="true" fullWidth="true" %}
```css
border-style: none;
border-style: hidden;
border-style: dotted;
border-style: dashed;
border-style: solid;
border-style: double;
border-style: groove;
border-style: ridge;
border-style: inset;
border-style: outset;

top and bottom | left and right
border-style: dotted solid;

top | left and right | bottom
border-style: hidden double dashed;

top | right | bottom | left
border-style: none solid dotted dashed;

### Global values
border-style: inherit;
border-style: initial;
border-style: revert;
border-style: revert-layer;
border-style: unset;
```
{% endcode %}

#### Example

{% code title="borders.html" overflow="wrap" lineNumbers="true" fullWidth="true" %}
```html
<pre class="b1">none</pre>
<pre class="b2">hidden</pre>
<pre class="b3">dotted</pre>
<pre class="b4">dashed</pre>
<pre class="b5">solid</pre>
<pre class="b6">double</pre>
<pre class="b7">groove</pre>
<pre class="b8">ridge</pre>
<pre class="b9">inset</pre>
<pre class="b10">outset</pre>
```
{% endcode %}

{% code title="" overflow="wrap" lineNumbers="true" fullWidth="true" %}
```css
pre {
  height: 80px;
  width: 120px;
  margin: 20px;
  padding: 20px;
  display: inline-block;
  background-color: palegreen;
  border-width: 5px;
  box-sizing: border-box;
}

.b1 {
  border-style: none;
}

.b2 {
  border-style: hidden;
}

.b3 {
  border-style: dotted;
}

.b4 {
  border-style: dashed;
}

.b5 {
  border-style: solid;
}

.b6 {
  border-style: double;
}

.b7 {
  border-style: groove;
}

.b8 {
  border-style: ridge;
}

.b9 {
  border-style: inset;
}

.b10 {
  border-style: outset;
}


```
{% endcode %}

{% code title="" overflow="wrap" lineNumbers="true" fullWidth="true" %}
```html
<!DOCTYPE html>
<html>
<head>
<style>
h1 {
  text-decoration-line: underline;
  text-decoration-style: solid;
}

h2 {
  text-decoration-line: underline;
  text-decoration-style: double;
}

h3 {
  text-decoration-line: underline;
  text-decoration-style: dotted;  
}

p.ex1 {
  text-decoration-line: underline;
  text-decoration-style: dashed;  
}

p.ex2 {
  text-decoration-line: underline;
  text-decoration-style: wavy;  
}

p.ex3 {
  text-decoration-line: underline;
  text-decoration-color: red;  
  text-decoration-style: wavy;  
}
</style>
</head>

<body>
<h1>Heading 1</h1>
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<p class="ex1">A paragraph.</p>
<p class="ex2">Another paragraph.</p>
<p class="ex3">Another paragraph.</p>
</body>
</html>
```
{% endcode %}

### list-style

`list-style` is shorthand for the following properties:

`list-style-type`

`list-style-position`

`list-style-image`

If one of the values are missing, the default value will be used.

Default value: `disc outside none`

#### Example

```css
list-style: disc inside;
list-style: disc outside;
list-style: circle inside;
list-style: circle outside;
list-style: square inside;
list-style: upper-roman inside;
list-style: lower-alpha inside;
list-style: disc inside url("sqpurple.gif");
list-style: disc inside url("smiley.gif");
```

## Transitions

| Name                         | Description                                                                                      |
| ---------------------------- | ------------------------------------------------------------------------------------------------ |
| `transition`                 | shorthand property for setting all four individual transition properties in a single declaration |
| `transition-delay`           | when transition starts                                                                           |
| `transition-duration`        | number of seconds or milliseconds a transition animation should take to complete                 |
| `transition-property`        | names of the CSS properties to which a transition effect should be applied to                    |
| `transition-timing-function` | how intermediate values of CSS properties are being affected by a transition will be calculated  |

For Safari browser, you should use the `-webkit-transition-` property to specify which properties should be animated and the `-webkit-transition-duration` for the duration.

**Example:**

```css
button {
  color: white;
  background: purple;
  transition: background 0.3s;
  -webkit-transition-property: background;
  -webkit-transition-duration: 0.3s;
}

button:hover {
  background: black;
}
```

## Keyframes

* lets an element gradually change from one style to another
* can change as many CSS properties you want, as many times as you want
* must first specify some keyframes for the animation
* keyframes hold what styles the element will have at certain times

### The `@keyframes` Rule

When you specify CSS styles inside the @keyframes rule, the animation will gradually change from the current style to the new style at certain times.

To get an animation to work, you must bind the animation to an element.

**Example 1**

* binds the "example" animation to `<div>`
* animation will last for 4 seconds
* gradually changes background-color of `<div>` from "red" to "yellow"
* specifies when the style will change by using keywords "from" and "to" (start to complete)

```html
<!DOCTYPE html>
<html>
<head>
<style> 
div {
  width: 100px;
  height: 100px;
  background-color: red;
  animation-name: example;
  animation-duration: 4s;
}

@keyframes example {
from {
  background-color: red;
}

to {
  background-color: yellow;
  }
}
</style>
</head>
<body>
<h1>CSS Animation</h1>
<div></div>
<p><b>Note:</b> When an animation is finished, it goes back to its original style.</p>
</body>
</html>
```

**Example 2**

* may also use percent
* 0% (start) and 100% (complete)
* using percent allows the use of as many style changes as you like
* will change `background-color` of `<div>` when animation is 25% complete, 50% complete, and 100% complete

```html
<!DOCTYPE html>
<html>
<head>
<style>
div {
  width: 100px;
  height: 100px;
  background-color: red;
  animation-name: example;
  animation-duration: 4s;
}

@keyframes example {
  0%   {
    background-color: red;
}

  25%  {
    background-color: yellow;
}

  50%  {
   background-color: blue;
}

  100% {
  background-color: green;
  }
}
</style>
</head>
<body>
<h1>CSS Animation</h1>
<div></div>
<p><b>Note:</b> When an animation is finished, it goes back to its original style.</p>
</body>
</html>
```

**Example 3**

* both `background-color` and `position` of `<div>` change when the animation is 25% complete, 50% complete, and 100% complete

```html
<!DOCTYPE html>
<html>
<head>
<style> 
div {
  width: 100px;
  height: 100px;
  background-color: red;
  position: relative;
  animation-name: example;
  animation-duration: 4s;
}

@keyframes example {
  0%   {
  background-color:red; 
  left:0px;
  top:0px;
}

  25%  {
  background-color:yellow; 
  left:200px; 
  top:0px;
}

  50%  {
  background-color:blue; 
  left:200px; 
  top:200px;
}

  75%  {
  background-color:green;
  left:0px;
  top:200px;
}

  100% {
  background-color:red; 
  left:0px; 
  top:0px;
  }
}
</style>
</head>
<body>
<h1>CSS Animation</h1>
<div></div>
<p><b>Note:</b> When an animation is finished, it goes back to its original style.</p>
</body>
</html>
```

| `@keyframes`                | Description                                                                                                                |
| --------------------------- | -------------------------------------------------------------------------------------------------------------------------- |
| `animation-name`            | name given element receiving animation                                                                                     |
| `animation-duration`        | how long an animation should take to complete; if not specified, animation will not occur; default value is 0s (0 seconds) |
| `animation-delay`           | delays start                                                                                                               |
| `animation-iteration-count` | number of times animation should run                                                                                       |
| `animation-direction`       | plays forward, backwards or in alternate cycles                                                                            |
| `animation-timing-function` | speed curve of the animation                                                                                               |
| `animation-fill-mode`       | style for the target element when the animation is not playing (before it starts, after it ends, or both)                  |
| `animation`                 |                                                                                                                            |

#### `animation-direction` value

`normal` - default; normal (forwards) `reverse`- reverse direction (backwards) `alternate` - forwards first, then backwards `alternate-reverse` - backwards first, then forwards

#### `animation-timing-function` value

`ease` - default; slow start, then fast, then end slowly `linear` - same speed from start to end `ease-in` - slow start `ease-out` - slow end `ease-in-out` - slow start and end `cubic-bezier(n,n,n,n)` - choose values in a cubic-bezier function

```html
<!DOCTYPE html>
<html>
<head>
<style> 
div {
  width: 100px;
  height: 50px;
  background-color: red;
  font-weight: bold;
  position: relative;
  animation: mymove 5s infinite;
}

#div1 {
  animation-timing-function: linear;
}

#div2 {
  animation-timing-function: ease;
}

#div3 {
  animation-timing-function: ease-in;
}

#div4 {
  animation-timing-function: ease-out;
}

#div5 {
  animation-timing-function: ease-in-out;
}

@keyframes mymove {
  from {left: 0px;}
  to {left: 300px;}
}
</style>
</head>
<body>
<h1>CSS Animation</h1>
<p>The animation-timing-function property specifies the speed curve of the animation. The following example shows some of the different speed curves that can be used:</p>
<div id="div1">linear</div>
<div id="div2">ease</div>
<div id="div3">ease-in</div>
<div id="div4">ease-out</div>
<div id="div5">ease-in-out</div>
</body>
</html>
```

#### `animation-fill-mode` value

`none` - default; animation will not apply any styles to element before or after  execu

`forwards` - element retains style values set by last keyframe (depends on `animation-direction` and `animation-iteration-count`

`backwards` - element gets style values set by first keyframe (depends on `animation-direction`), and retain this during `animation-delay period`

`both` - both forwards and backwards, extending the animation properties in both directions

#### Animation Shorthand Property

```html
<!DOCTYPE html>
<html>
<head>
<style> 
div {
  width: 100px;
  height: 100px;
  background-color: red;
  position: relative;
  animation-name: example;
  animation-duration: 5s;
  animation-timing-function: linear;
  animation-delay: 2s;
  animation-iteration-count: infinite;
  animation-direction: alternate;
}

@keyframes example {
0%   {
  background-color:red; 
  left:0px; 
  top:0px;
}
  25%  {
  background-color: yellow; 
  left:200px; 
  top:0px;
}
50%  {
  background-color:blue;   
  left:200px; 
  top:200px;
}
75%  {
  background-color:green;
  left:0px; 
  top:200px;
}
  100% {
  background-color:red; 
  left:0px; 
  top:0px;
  }
}
</style>
</head>
<body>
<h1>CSS Animation</h1>
<p>This example uses six of the animation properties:</p>
<div></div>
</body>
</html>
```

**Use shorthand:**

```html
<!DOCTYPE html>
<html>
<head>
<style> 
div {
  width: 100px;
  height: 100px;
  background-color: red;
  position: relative;
  animation: myfirst 5s linear 2s infinite alternate;
}

@keyframes myfirst {
  0%   {
  background-color:red; 
  left:0px; 
  top:0px;
}
25%  {
  background-color:yellow; 
  left:200px; 
  top:0px;
}
50%  {
  background-color:blue; 
  left:200px;
  top:200px;
}
75%  {
  background-color:green;
  left:0px;
  top:200px;
}
100% {
  background-color:red; 
  left:0px;
  top:0px;
  }
}
</style>
</head>
<body>
<h1>CSS Animation</h1>
<p>This example uses the shorthand animation property:</p>
<div></div>
</body>
</html>
```
