# JavaScript

export CERTS\_DIR="/home/ubuntu/.certbot/config/live/${DOMAIN}" export PROJECTS\_DIR="/home/ubuntu"

docker run -d\
\--name my-environment\
\-p 3443:3443\
\-v "${CERTS\_DIR}/fullchain.pem:/gitlab-rd-web-ide/certs/fullchain.pem"\
\-v "${CERTS\_DIR}/privkey.pem:/gitlab-rd-web-ide/certs/privkey.pem"\
\-v "${PROJECTS\_DIR}:/projects"\
registry.gitlab.com/gitlab-org/remote-development/gitlab-rd-web-ide-docker:0.2-alpha\
\--log-level warn --domain "${DOMAIN}" --ignore-version-mismatch
